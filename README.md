## Introduction

This is a simple web widget for ID06-LVP allowing to perform live azimuthal integration of 2D powder patterns, display them as raw 2D data, integrated powder pattern (1D), 2D view of the evolution of the diffraction, and provide some basic analysis (plot crystal structure psoitions and basic profile fitting).

This uses mainly [pyFAI](https://pyfai.readthedocs.io) for the azimuthal integration, and [pyobjcryst](https://pyobjcryst.readthedocs.io/en/latest/examples/index.html) for some crystallographic analysis.

Example screenshot:
![Wipow screenshot](https://gitlab.esrf.fr/favre/wipow/-/raw/master/doc/wipow-screenshot-voila.png)

## Installation

### Dependencies & conda environment

Dependencies should be installed first using conda (or better, mamba)
with the conda-forge repository.

Assuming you have a conda environment with mamba pre-installed, use:

``` sh
conda config --add channels conda-forge
conda config --set channel_priority strict

mamba create -n wipow python=3.10 numpy matplotlib ipympl jupyter notebook numexpr pyfai \
    ipywidgets pyobjcryst py3dmol pyopencl git setuptools voila tifffile
```

Notes:
* The first two lines above are only necessary if you do not have the conda-forge channel available
by default, which is the case if you use the 
[mamba-forge distribution](https://github.com/conda-forge/miniforge/releases>)  (highly recommended !)
* python>=3.8 is required to have pyobjcryst>2.2.4, using 3.10 is recommended to ensure
updates of pyobjcryst as long as this version of python is supported by conda-forge.

### wipow installation
The package itself can be installed with  `pip install .` from the root of the downloaded wipow directory.

Alternatively, it can be installed directly (no local download needed) using:

  `pip install git+https://gitlab.esrf.fr/favre/wipow.git`

