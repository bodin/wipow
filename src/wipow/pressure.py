import math
from importlib import resources as impresources
from . import resources as p_cal_resources
import numpy as np
import scipy.integrate as integrate
import scipy.optimize as optimize

'''
In ./resources/DD07.csv file (after Dorogokupets & Dewaele 2007):

K0 [GPa] - bulk modulus at reference isotherm and x=1
Kp0 - pressure derivative of bulk modulus at reference isotherm and x=1
TD0 [K] - Debye temperature at reference isotherm and x=1
g0 - Gruneisen parameter at reference isotherm and x=1
ginf - Gruneisen parameter at x=0
beta - Gruneisen parameter volume dependence
a [1/K] - parameter of intrinsic anharmonicity
m - parameter of intrinsic anharmonicity
e [1/K] - parameter of free electrons
g - parameter of free electrons
n - number of atoms in formula
V0 [m**3/mol] - molar volume at reference isotherm and x=1
Z - formula units in unit cell
T0 [K] - reference isotherm

In ./resources/S16.csv file (after Sokolova et al. 2016):

n - number of atoms in formula
Za - atomic number (for compounds see reference)
V0 [m**3/mol] - molar volume at reference isotherm and x=1
K0 [GPa] - bulk modulus at reference isotherm and x=1
Kp0 - pressure derivative of bulk modulus at reference isotherm and x=1
TE10 [K] - first Einstein temperature at reference isotherm and x=1
m1 - first Einstein member
TE20 [K] - second Einstein temperature at reference isotherm and x=1
m2 - second Einstein member
delta - additive normalizing constant
t - model number
a [1/K] - parameter of intrinsic anharmonicity
m - parameter of intrinsic anharmonicity
e [1/K] - parameter of free electrons
g - parameter of free electrons
Z - formula units in unit cell
T0 [K] - reference isotherm

x = V/V0
'''


# pressure at reference isotherm [GPa] (Vinet EoS):
def P0_V(x, K0, Kp0):
    y = np.power(x, 1. / 3.)
    return 3 * K0 * y ** (-2) * (1 - y) * math.exp((1 - y) * 1.5 * (Kp0 - 1))


# pressure, K and Kp at reference isotherm [GPa] (EoS from Holzapfel 2001):
def P0_H(x, n, Za, V0, K0, Kp0):
    X = x ** (1 / 3)
    PFG0 = 100.36e-9 * (n * Za / V0) ** (5 / 3)  # Fermi gas pressure at x=1 [GPa]
    c0 = -math.log(3 * K0 / PFG0)
    c2 = 1.5 * (Kp0 - 3) - c0
    P = 3 * K0 * X ** (-5) * (1 - X) * math.exp(c0 * (1 - X)) * (1 + c2 * X * (1 - X))
    K = K0 * X ** (-5) * math.exp(c0 * (1 - X)) * ((5 - 4 * X) * (1 + c2 * X * (1 - X))
                                                   + c0 * X * (1 - X) * (1 + c2 * X * (1 - X)) -
                                                   (1 - X) * (c2 * X - 2 * c2 * X ** 2))
    Kp = 1 / 3 * X * (c0 + 5 / X + ((2 * X - 1) * (c0 + 4 * c2) - 2 * (3 * c2 * X ** 2 - 2 * c2 * X - 2)) /
                      ((1 + c2 * X * (1 - X)) * (5 - 4 * X + c0 * X - c0 * X ** 2) - c2 * X * (1 - X) ** 2))
    return (P, K, Kp)


# thermal pressure [GPa] (Dorogokupets & Dewaele 2007)
def Pth_DD07(x, T, TD0, g0, ginf, beta, a, m, e, g, n, V0):
    R = 8.3145
    # Debye temperature [K]:
    TD = TD0 * x ** (-ginf) * math.exp((g0 - ginf) / beta * (1 - x ** beta))
    t = TD / T
    # Debye function (n = 3):
    D3 = 3 * t ** (-3) * integrate.quad(lambda z: z ** 3 / (math.exp(z) - 1), 0, t)[0]
    V = V0 * x
    # quasiharmonic part:\
    Pqh = 3 * n * R * (3 / 8 * TD + T * D3) * (ginf + (g0 - ginf) * x ** beta) / V
    # contribution of intrinsic anharmonicity:
    Panh = 3 / 2 * n * R * a * x ** m * T ** 2 * m / V
    # contribution of free electrons:
    Pel = 3 / 2 * n * R * e * x ** g * T ** 2 * g / V
    return (Pqh + Panh + Pel) * 1e-9  # conversion to GPa


# thermal pressure [GPa] (Sokolova et al. 2016)
def Pth_S16(x, T, n, Za, V0, K0, Kp0, TE10, m1, TE20, m2, delta, t, a, m, e, g):
    R = 8.3145
    P, K, Kp = P0_H(x, n, Za, V0, K0, Kp0)
    # Gruneisen parameter at reference isotherm:
    gamma = (Kp / 2 - 1 / 6 - t / 3 * (1 - P / 3 / K)) / (1 - 2 * t * P / 3 / K) + delta
    # Debye temperatures at reference isotherm:
    TEV1 = TE10 * x ** (1 / 6 - delta) * K0 ** (-1 / 2) * (K - 2 * t * P / 3) ** (1 / 2)
    TEV2 = TE20 * x ** (1 / 6 - delta) * K0 ** (-1 / 2) * (K - 2 * t * P / 3) ** (1 / 2)
    # Debye temperatures:
    TE1 = TEV1 * math.exp(1 / 2 * a * x ** m * T)
    TE2 = TEV2 * math.exp(1 / 2 * a * x ** m * T)
    V = V0 * x
    return (m1 * R * (gamma - m / 2 * a * x ** m * T) / V * TE1 / (math.exp(TE1 / T) - 1) +
            m2 * R * (gamma - m / 2 * a * x ** m * T) / V * TE2 / (math.exp(TE2 / T) - 1) +
            3 / 2 * n * R * e * x ** g * T ** 2 * g / V) * 1e-9  # conversion to GPa


def load_dd07():
    """Load pressure calculation data from the DD07 file"""
    src = impresources.files(p_cal_resources) / 'DD07.csv'
    data = np.loadtxt(src, dtype=str, delimiter=',')
    db = {}
    for i in data:
        db[i[0]] = i[1:].astype(float)
    return db


def load_s16():
    """Load pressure calculation data from the S16 file"""
    src = impresources.files(p_cal_resources) / 'S16.csv'
    data = np.loadtxt(src, dtype=str, delimiter=',')
    db = {}
    for i in data:
        db[i[0]] = i[1:].astype(float)
    return db


# PRESSURE CALCULATOR [GPa]:
# eos - DD07 (Dorogokupets & Dewaele 2007), S16 (Sokolova et al. 2016)
# st = standard from .csv
# VA3 - cell volume [A**3]
# T - temperature [K]
def P(eos, st, VA3, T):
    if eos == 'DD07':
        db = load_dd07()
        V = VA3 / 1e30 * 6.02214e23 / db[st][12]
        V0 = db[st][11]
        x = V / V0
        return P0_V(x, *db[st][:2]) \
            - Pth_DD07(x, db[st][-1], *db[st][2:-2]) \
            + Pth_DD07(x, T, *db[st][2:-2])

    if eos == 'S16':
        db = load_s16()
        V = VA3 / 1e30 * 6.02214e23 / db[st][15]
        V0 = db[st][2]
        x = V / V0
        return P0_H(x, *db[st][:5])[0] \
            - Pth_S16(x, db[st][-1], *db[st][:15]) \
            + Pth_S16(x, T, *db[st][:15])

    if eos == 'BN':
        B0 = 27.6 - 0.81 * 1e-2 * (T - 298)
        B00 = 10.5 + 0.0016 * (T - 298)
        V0 = 36.500952 * math.exp(35.259 * 1e-6 * T +
                                  (0.2095 * 1e-8 * T ** 2) / 2 +
                                  (1.236 * 1e-11 * T ** 3) / 3 -
                                  (7.1994 * 1e-15 * T ** 4) / 4)
        f = 1 / 2 * ((VA3 / V0) ** -2 / 3 - 1)
        return (3 * B0 * f * (1 + 2 * f) ** 5 / 2 * (1 - 3 / 2 * (4 - B00) * f))

    if eos == 'Re':
        V0 = 29.4087
        x = V0 / VA3
        return (3 / 2 * 360 * (x ** (7 / 3) - x ** (5 / 3)) *
                (1 + 3 / 4 * (4.5 - 4) * (x ** (2 / 3) - 1)) +
                (0.00776 - 0.00815 * math.log(x)) * (T - 298))


# cross-calibration using isochores of two standards:
def cross(eos1, eos2, st1, st2, V1, V2):
    sol = optimize.fsolve(lambda T: P(eos1, st1, V1, T)
                                    - P(eos2, st2, V2, T), 500)
    Tcr = sol[0]
    Pcr = (P(eos1, st1, V1, Tcr) + P(eos2, st2, V2, Tcr)) / 2
    return (Tcr, Pcr)


# cross-calibration uncertainty:
def sig(eos1, eos2, st1, st2, V1, V2, sig1, sig2):
    tdisp = []
    pdisp = []
    for i in (-sig1, sig1):
        for j in (-sig2, sig2):
            tdisp.append(cross(eos1, eos2, st1, st2, V1 + i, V2 + j)[0])
            pdisp.append(cross(eos1, eos2, st1, st2, V1 + i, V2 + j)[1])
    return ((max(tdisp) - min(tdisp)) / 2,
            (max(pdisp) - min(pdisp)) / 2)


def Psig(eos, st, V, T, sig):
    pdisp = []
    for i in (-sig, sig):
        pdisp.append(P(eos, st, V + i, T))
    return ((max(pdisp) - min(pdisp)) / 2)
