# -*- coding: utf-8 -*-

#   (c) 2022-present : ESRF-European Synchrotron Radiation Facility
#       authors:
#         Vincent Favre-Nicolin, favre@esrf.fr

""" Simple widget for the live visualisation of powder diffraction patterns
with azimuthal integration and basic display of crystal phases."""

__all__ = ['ID06LVP_LiveWidget']

import os
import timeit
import re
import io
import json
import base64
import hashlib
import glob
import logging
import asyncio
import traceback
import socket
from urllib.error import URLError
import gzip
import numpy as np
import hdf5plugin
from silx.io import h5py_utils
from blissdata.h5api import dynamic_hdf5
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import matplotlib.ticker as ticker
from matplotlib.legend import Legend
import ipywidgets
import pyFAI
from pyFAI.io.ponifile import PoniFile
from pyFAI.azimuthalIntegrator import AzimuthalIntegrator
import fabio
from tifffile import imwrite
from IPython.display import display, HTML, clear_output

from pyobjcryst.crystal import Crystal, create_crystal_from_cif
from .powderpattern import PowderPattern

# from ipyfilechooser import FileChooser
from pyobjcryst._pyobjcryst import XMLCrystFileSaveGlobal
from pyobjcryst.globals import gCrystalRegistry, gPowderPatternRegistry
from .utils import NoEventContextManager, OutputWidgetHandler
from .crystal import CrystalWidget
from .pressure import load_s16, load_dd07, P as pressure_calc


class DownloadButton(ipywidgets.Button):
    """Download button with dynamic content.

    The content is generated using a callback when the button is clicked.

    Note: this works in a jupyter notebook but not in jupyterlab
    Inspired by:
    - https://stackoverflow.com/questions/61708701/how-to-download-a-file-using-ipywidget-button
    - https://github.com/voila-dashboards/voila/issues/711
    """

    def __init__(self, id06_widget, extension, **kwargs):
        if 'WIPOW_CWD' in os.environ:
            os.chdir(os.environ['WIPOW_CWD'])
        super().__init__(**kwargs)
        self.id06_widget = id06_widget
        self.extension = extension
        self.on_click(self.__on_click)

    def __on_click(self, b):
        if self.extension == '.xmlgz':
            contents = self._content_xmlgz()
        elif self.extension == '.xye':
            contents = self._content_xye()
        elif self.extension == '.cif':
            contents = self._content_cif()
        elif self.extension == '.tif':
            contents = self._content_tif2d()
        digest = hashlib.md5(contents).hexdigest()  # bypass browser cache
        b64 = base64.b64encode(contents)
        payload = b64.decode()
        if self.extension != '.tif':
            filename = os.path.split(self.id06_widget.data_src)[1] % self.id06_widget.slider_frame.value
            filename = os.path.splitext(filename)[0] + self.extension
        else:
            filename = os.path.split(self.id06_widget.data_src)[1] % self.id06_widget.slider_frame.max
            filename = os.path.splitext(filename)[0] + "_2D" + self.extension
        id = f'dl_{digest}'
        data_url = f'data:text/csv;charset=utf-8;base64,{payload}'
        js_code = f"""
            var a = document.createElement('a');
            a.setAttribute("id", "{id}");
            a.setAttribute('download', '{filename}');
            a.setAttribute('href', '{data_url}');
            a.click()
        """
        with self.id06_widget.download_output:
            clear_output()
            display(HTML(f'<script>{js_code}</script>'))

    def _content_xmlgz(self):
        """ Export as an xmlgz (compressed xml) data format used by Fox.
        This includes the powder diffraction data and the crystalline phases."""
        # Kludge: empty global powder pattern and crystal registry to make sure only relevant objects are saved
        gPowderPatternRegistry.DeRegisterAll()
        gPowderPatternRegistry.Register(self.id06_widget.powder_pattern)
        gCrystalRegistry.DeRegisterAll()
        for c in self.id06_widget.crystal_phases:
            if c['toggle'].value:
                gCrystalRegistry.Register(c['crystal'])
        # Update the background and scale factor before export
        self.id06_widget.powderpattern_background.UnFixAllPar()
        self.id06_widget.powderpattern_background.OptimizeBayesianBackground()
        self.id06_widget.powder_pattern.FitScaleFactorForIntegratedRw()
        s = io.StringIO()
        XMLCrystFileSaveGlobal(s)
        contents = gzip.compress(bytes(s.getvalue().encode('utf-8')))
        return contents

    def _content_xye(self):
        """three-columns (2theta, Iobs, sigma) data with 6 lines comment header,
        suitable for import in Fullprof, GSASII or Topas"""
        i0 = self.id06_widget.slider_frame.value
        s = io.StringIO()
        if False:
            # Header as in GSAS-II
            s.write("/*\n")  # Header delimiter used by topas...
            s.write("# Original data: %s\n" % (self.id06_widget.data_src % i0))
            s.write("# Azimuthal integration: pyFAI\n")
            s.write("# Wavelength: %8.5f A\n" % (self.id06_widget.azi.get_wavelength() * 1e10))
            s.write("#\n")
            s.write("# Note: due to detector gaps, 2theta step is not constant\n")
            s.write("*/\n")
        for i in range(len(self.id06_widget.tth)):
            s.write("%12.5f %15.6f %15.6f\n" % (self.id06_widget.tth[i], self.id06_widget.iobs2d[i0, i],
                                                self.id06_widget.iobs2d_sigma[i0, i]))
        return s.getvalue().encode('utf-8')

    def _content_cif(self):
        """Powder diffraction data as a CIF file"""
        i0 = self.id06_widget.slider_frame.value
        s = io.StringIO()
        s.write("# Original data: %s\n" % (self.id06_widget.data_src % i0))
        s.write("# Azimuthal integration: pyFAI\n")
        # s.write("# Wavelength: %8.5f A\n" % (self.id06_widget.azi.get_wavelength() * 1e10))
        s.write("#\n")
        s.write("# Note: due to detector gaps, 2theta step is not constant\n")
        s.write("\n")
        s.write("\n")
        # May need to check against exotic characters/spaces ?
        s.write("data_%s\n" % os.path.split(self.id06_widget.data_src % i0)[1])
        s.write("\n")
        s.write("_pd_proc_wavelength %12.6f\n" % (self.id06_widget.azi.get_wavelength() * 1e10))
        s.write("\n")
        s.write("_pd_block_id %s\n" % os.path.split(self.id06_widget.data_src % i0)[1])
        s.write("\n")
        s.write("loop_\n")
        s.write("      _pd_data_point_id\n")
        s.write("      _pd_proc_2theta_corrected\n")
        s.write("      _pd_proc_intensity_net\n")
        s.write("      _pd_proc_ls_weight\n")
        for i in range(len(self.id06_widget.tth)):
            sig = self.id06_widget.iobs2d_sigma[i0, i]
            w = 1 / sig ** 2 if sig > 0 else 0
            s.write("%5d %12.5f %15.6f %15.6f\n" % (i, self.id06_widget.tth[i], self.id06_widget.iobs2d[i0, i], w))
        s.write("\n")
        return s.getvalue().encode('utf-8')

    def _content_tif2d(self):
        """Tiff file with the 2D floating-point data. Normalised if normalisation is currently active.
        Note the x coordinates are not regular dues to gaps, so saving a .xye file is required
        to know the actual 2theta values."""
        iobs2d = self.id06_widget.get_iobs2d()
        b = io.BytesIO()
        imwrite(b, iobs2d.astype(np.float32))
        return b.getvalue()


class ID06LVP_LiveWidget(ipywidgets.VBox):
    def __init__(self, poni_filename=None, data_src=None,
                 processed_data_filename=None, processed_data_scan=None,
                 _debug_simulate_live=False):
        super().__init__()
        # Mask for the detector
        self.mask = None
        self.idx_nomask = None  # index of valid points after 1D integration
        self.radial_range = None  # Needed to make sure the integration is always the same
        # Tabs
        self.tabs = ipywidgets.Tab()
        # logger
        self.logger = logging.getLogger(__name__)
        # self.logger.
        self.logger.propagate = False  # Disables stderr (and notebook) output
        self.log_handler = OutputWidgetHandler()
        self.logger.addHandler(self.log_handler)
        self.logger.setLevel(logging.INFO)
        # self.logger.setLevel(logging.DEBUG)
        self.log_output = self.log_handler.out
        # Dummy output to enable download
        self.download_output = ipywidgets.Output()
        # display(self.download_output)

        self.children = [self.tabs, self.log_output, self.download_output]
        self.logger.info("Creating id06lvp_live_view")

        # Configuration
        self.processed_data_filename = None
        self.processed_data_scan = 0
        self.data_src = data_src  # "path/to/data%04d.edf"
        self.poni_filename = poni_filename
        self.detector_geometry_filename = None
        # Azimuthal integrator
        self.azi = None

        if _debug_simulate_live:
            self._ct_live = 50  # Start live up to frame 50, the add progressively
        else:
            # DEVELOPMENT - set to <0 to disable
            self._ct_live = -1  # will be increased to make scans appear progressively for testing

        # Loaded frames
        self.v_i = []
        # 1D pattern X coordinates
        self.tth = None
        self.iobs2d = None
        self.iobs2d_sigma = None
        self.iobs_raw = None

        # Plot objects
        self.plot_2draw_fig = None
        self.plot_2draw_obj = None
        self.plot_2draw_ax = None
        self.plot_2d_fig = None
        self.plot_2d_obj = None
        self.plot_1d_fig = None
        self.plot_1d_obj = None
        # We use this to know if user zoomed in
        self._last_iobs2d_len = 0

        # For pressure calc
        self._pressure_values = {}
        self.plot_pressure_fig = None
        self.plot_pressure_ax = None
        self.plot_pressure_obj = None

        # pyobjcryst PowderPattern object with all relevant phases
        self.powder_pattern = None
        # Background component for the powder pattern (left at zero for future or manual use)
        self.powderpattern_background = None
        # CIFs for the crystal phases (can be a file path, or an URL)
        self.crystal_phases = []
        # Const profile width, if we want to compute it
        # self.powder_pattern_w = 0.005

        self.tab_live = self.make_tab_live()
        self.tab_config = self.make_tab_config(poni_filename, data_src,
                                               processed_data_filename=processed_data_filename,
                                               processed_data_scan=processed_data_scan)
        self.tab_analysis = self.make_tab_analysis()

        # Can be used to disable update event handling
        self._enable_events = True

        # Timing
        self.t0 = timeit.default_timer()

        # Init the powder pattern (now we have self.tth)
        self.init_power_pattern()

        if self.tth is not None:
            self.tabs.children = self.tab_live, self.tab_analysis, self.tab_config
            self.tabs.set_title(0, "Live integration")
            self.tabs.set_title(1, "Analysis")
            self.tabs.set_title(2, "Configuration")
        else:
            self.tabs.children = self.tab_config,
            self.tabs.set_title(0, "Configuration")
            self.logger.info("Please configure the poni / data_src or processed data file to access the live view")
        # self.tabs.set_title(3, "Analysis")

        self.logger.info("Creating id06lvp_live_view... done")

    def __del__(self):
        self.toggle_live.value = False
        self.toggle_live.value = False  # Not sure if this will help removing double messages when re-starting the cell
        super().__del__()

    def init_data(self):
        """Init all stored data, restarting from empty arrays"""
        if (self.poni_filename is None or self.data_src is None) and \
                (self.config_processed_data_filename is None or self.config_processed_scan is None):
            self.logger.info("No poni filename / data_src / processed data given yet ")
            return
        self.logger.info("Initialising data")
        # Loaded frames
        self.v_i = []
        # 1D pattern X coordinates
        self.tth = None
        self.iobs2d = None
        self.iobs2d_sigma = None
        self.iobs_raw = None
        v_i = self.scan_data_folder()
        self.logger.debug(f"Initialising data 1: {v_i}")
        self.toggle_live.value = False
        with NoEventContextManager(self):
            self.slider_frame.value = 0
            self.slider_frame.max = v_i[-1]
            self.range_normalisation.value = [-1, -1]
            self.range_normalisation.min = -1
            self.range_normalisation.max = -1
            self.range_cmap_raw.value = [-1, -1]
            self.range_cmap_raw.min = -1
            self.range_cmap_raw.max = -1
            self.range_cmap_2d.value = [-1, -1]
            self.range_cmap_2d.min = -1
            self.range_cmap_2d.max = -1
        self.logger.debug(f"Initialising data: {self.processed_data_filename}:{self.processed_data_scan}")
        if self.processed_data_filename is not None:
            self.logger.info("Creating Azimuthal integrator from processed data configuration")
            # input azimuthal integration parameters
            try:
                with h5py_utils.File(self.processed_data_filename, 'r', locking=False) as h:
                    s = f"{self.processed_data_scan}.1/p900kw_integrate/configuration/data"
                    try:
                        # TODO: do we need to try again ?
                        poni = h[s][()].decode()
                        poni = json.loads(poni)
                    except Exception:
                        self.logger.error(f"Failed to extract azimuthal integration parameters: "
                                          f"{self.processed_data_filename}::{s}")
            except Exception as ex:
                self.logger.error(f"Failed opening hdf5 file: {self.processed_data_filename}")
                raise RuntimeError(f"Failed either opening processed data file ["
                                   f"{self.processed_data_filename}] or reading the "
                                   f"azimuthal integration parameters inside that file")
            for k in list(poni.keys()):
                if k not in ['dist', 'distance', 'poni1', 'poni2', 'rot1', 'rot2', 'rot3', 'wavelength', 'detector']:
                    del poni[k]
            self.logger.debug(f"Creating poni file from: {poni}")
            self.azi = AzimuthalIntegrator(**poni)
            self.logger.debug(f"Finished creating AzimuthalIntegrator from processed data file parameters")
        elif self.detector_geometry_filename is not None:
            self.logger.info("Creating Azimuthal integrator with PONI file (%s) and detector geometry (%s)" %
                             (self.poni_filename, self.detector_geometry_filename))
            poni = PoniFile(self.poni_filename).as_dict()
            for k in list(poni.keys()):
                if k.lower() not in ['dist', 'distance', 'poni1', 'poni2', 'rot1', 'rot2', 'rot3', 'wavelength']:
                    del poni[k]
            poni['detector'] = pyFAI.detector_factory(self.detector_geometry_filename)
            print(poni)
            self.azi = AzimuthalIntegrator(**poni)
        else:
            self.logger.info("Creating Azimuthal integrator with PONI file (%s) and no detector geometry" %
                             self.poni_filename)
            self.azi = pyFAI.load(self.poni_filename)

        # self.update_raw()
        # self.update_1d()
        # self.update_2d()
        # load frame #0
        self.load_i()

        self.logger.info("Initialising data... done")

    def make_tab_live(self):
        """ Main graphical display tab
        """
        box0 = ipywidgets.HBox()
        box1 = ipywidgets.VBox()
        box2 = ipywidgets.VBox()

        # 2D raw plot
        with plt.ioff():
            self.plot_2draw_fig, self.plot_2draw_ax = plt.subplots(constrained_layout=True, figsize=(12, 1.5))
            self.plot_2draw_obj = None
            plt.axis('off')
            self.plot_2draw_fig.set_layout_engine(layout='tight')
            self.plot_2draw_fig.canvas.header_visible = False
            # self.plot_2draw_fig.canvas.footer_visible = False
            # self.plot_2draw_fig.canvas.manager.toolmanager.remove_tool('forward')
            # self.plot_2draw_fig.canvas.manager.toolmanager.remove_tool('backward')
            # display(self.plot_2draw_fig.canvas)  # without .canvas only the plot is displayed
            # self.plot_2draw_fig.canvas.draw()

            # 1D plot
            self.plot_1d_fig, self.plot_1d_ax = plt.subplots(constrained_layout=True, figsize=(12, 4))
            self.plot_1d_obj = None
            self.plot_1d_fig.set_layout_engine(layout='tight')
            # w_plot_1d.plot1d, = w_plot_1d.ax.plot(tth, iobs)
            # display(self.plot_1d_fig.canvas)  # without .canvas only the plot is displayed
            self.plot_1d_fig.canvas.header_visible = False
            # self.plot_1d_fig.canvas.footer_visible = False
            # self.plot_1d_fig.canvas.draw()

            # 2D plot
            self.plot_2d_fig, self.plot_2d_ax = plt.subplots(figsize=(12, 5))  # constrained_layout=True,
            self.plot_2d_obj = None
            self.plot_2d_fig.set_layout_engine(layout='tight')
            # plt.axis('off')
            self.plot_2d_fig.canvas.header_visible = False
            # self.plot_2d_fig.canvas.footer_visible = False
            # display(self.plot_2d_fig.canvas)  # without .canvas only the plot is displayed
            # self.plot_2d_onclick_connect = self.plot_2d_fig.canvas.mpl_connect('button_press_event',
            #                                                                   self.on_click_2d)

        # frame selection
        self.slider_frame = ipywidgets.IntSlider(value=0, min=0, max=0, step=1, description="Frame:",
                                                 disabled=False, continuous_update=True, orientation="horizontal",
                                                 readout=True, readout_format='d',
                                                 layout=ipywidgets.Layout(width="auto"))
        self.slider_frame.observe(self.on_slider_frame)

        # Toggle buttons
        self.toggle_live = ipywidgets.ToggleButton(value=False, description='Live', disabled=False,
                                                   tooltip='Enable live update of 1D & 2D frames',
                                                   icon='check',
                                                   layout=ipywidgets.Layout(width="auto"))
        self.toggle_autoscale_1d = ipywidgets.ToggleButton(value=True, description='Autoscale (1D)', disabled=False,
                                                           tooltip='Autoscale min/max for 1D plot',
                                                           icon='check',
                                                           layout=ipywidgets.Layout(width="auto"))
        # 2theta range for data normalisation
        label_range_norm = ipywidgets.Label(r"Normalisation \(2\theta\) range (min=max to disable):",
                                            layout=ipywidgets.Layout(width="auto", align="center"))
        self.range_normalisation = ipywidgets.FloatRangeSlider(value=[-1, -1],
                                                               min=-1, max=-1,
                                                               step=0.1,
                                                               description="",
                                                               tooltip="Normalisation 2theta range "
                                                                       "(min=max to disable)",
                                                               continuous_update=False)
        # Colourmap choice
        self.choice_cmap = ipywidgets.Dropdown(options=['gnuplot2', 'Greys', 'Greys_r', 'hot',
                                                        'jet', 'magma', 'viridis'],
                                               value='Greys_r', description='Colourmap:',
                                               disabled=False, layout=ipywidgets.Layout(width="auto"))
        # 2D raw color range
        label_range_raw = ipywidgets.Label(r"Raw 2D colourmap range (log10):",
                                           layout=ipywidgets.Layout(width="auto", align="center"))
        self.range_cmap_raw = ipywidgets.FloatRangeSlider(value=[-1, -1],
                                                          min=-1, max=-1,
                                                          step=0.1,
                                                          description="",
                                                          continuous_update=False)
        # 2D color range
        label_range_2d = ipywidgets.Label(r"2D colourmap range (log10):",
                                          layout=ipywidgets.Layout(width="auto", align="center"))
        self.range_cmap_2d = ipywidgets.FloatRangeSlider(value=[-1, -1],
                                                         min=-1, max=-1,
                                                         step=0.1,
                                                         description="",
                                                         continuous_update=False)
        # 1D plot scale choice
        self.choice_1d_yscale = ipywidgets.Dropdown(options=['linear', 'log', 'sqrt'],
                                                    value='sqrt', description='1D plot scale:',
                                                    disabled=False, layout=ipywidgets.Layout(width="auto"))

        # Download buttons
        self.download_button_fox = DownloadButton(self, ".xmlgz", description='Download (Fox/.xmlgz)',
                                                  layout=ipywidgets.Layout(width="auto"))
        self.download_button_cif = DownloadButton(self, ".cif", description='Download CIF data',
                                                  layout=ipywidgets.Layout(width="auto"))
        self.download_button_xye = DownloadButton(self, ".xye", description='Download .xye',
                                                  layout=ipywidgets.Layout(width="auto"))
        self.download_button_tif2d = DownloadButton(self, ".tif", description='Download (2D, float) as .tif',
                                                    layout=ipywidgets.Layout(width="auto"))
        self.progress_bar = ipywidgets.IntProgress(value=0, min=0, max=10, style={'bar_color': 'green'},
                                                   orientation='horizontal',
                                                   layout=ipywidgets.Layout(width="auto"))

        # box for crystal structures
        self.crystal_vbox = ipywidgets.VBox()
        self.crystal_tuning_toggle = ipywidgets.ToggleButton(value=False, description='Fine manual tuning (0.001)',
                                                             disabled=False,
                                                             tooltip='Toggle 0.001/0.01 lattice tuning',
                                                             icon='check',
                                                             layout=ipywidgets.Layout(width="auto"))

        self.crystal_vbox.children = [self.crystal_tuning_toggle]

        # Organise view
        box0.children = [box1, box2]

        box1.children = [self.plot_2draw_fig.canvas, self.plot_1d_fig.canvas,
                         self.slider_frame, self.plot_2d_fig.canvas]

        box2.children = [self.toggle_live, self.toggle_autoscale_1d,
                         label_range_norm, self.range_normalisation, self.choice_cmap,
                         label_range_raw, self.range_cmap_raw, label_range_2d, self.range_cmap_2d,
                         self.choice_1d_yscale,
                         self.download_button_fox, self.download_button_cif, self.download_button_xye,
                         self.download_button_tif2d,
                         self.crystal_vbox, self.progress_bar]
        # Event handling
        self.toggle_live.observe(self.on_live)
        self.toggle_autoscale_1d.observe(self.on_autoscale_1d)
        self.range_normalisation.observe(self.on_range_norm)
        self.choice_cmap.observe(self.on_change_colors)
        self.range_cmap_2d.observe(self.on_change_colors)
        self.range_cmap_raw.observe(self.on_change_colors)
        self.choice_1d_yscale.observe(self.on_change_1d_scale)
        self.crystal_tuning_toggle.observe(self.on_change_crystal_tuning)

        return box0

    def make_tab_analysis(self):
        box0 = ipywidgets.HBox()

        # To plot the powder pattern
        analysis_box = ipywidgets.VBox()
        with plt.ioff():
            self.analysis_fig, analysis_ax = plt.subplots(constrained_layout=True, figsize=(12, 6))
            self.analysis_fig.canvas.header_visible = False
            # display(self.analysis_fig.canvas)
        analysis_warning = ipywidgets.HTML(
            value="<b>WARNING</b>: profile fitting automatically with multiple phases can be unstable. "
                  "If that happens, export the pattern (Fox, cif, xye..) for analysis in an external tool.<br>"
                  "Note: reloading the configuration can be used to reset the crystalline phases; "
                  "as long as the poni and data file paths have not changed, data will be unchanged.",
            layout=ipywidgets.Layout(width='auto', height='auto'))
        # Same slider as in live tab, practical to serialise analysis
        analysis_box.children = [analysis_warning, self.analysis_fig.canvas, self.slider_frame]

        # Menu for profile fitting
        self.analysis_menu = ipywidgets.VBox()
        opt = ['all phases']
        for i in range(len(self.crystal_phases)):
            opt.append("%d-%s" % self.crystal_phases[i]["crystal"].GetName())
        self.analysis_fit_dropdown = ipywidgets.Dropdown(options=opt,
                                                         value='all phases',
                                                         description='Phase to fit:',
                                                         disabled=False, layout=ipywidgets.Layout(width="auto"))
        self.analysis_fit_button = ipywidgets.Button(description='Le Bail + quick fit',
                                                     disabled=False, layout=ipywidgets.Layout(width="auto"),
                                                     button_style='',  # 'success', 'info', 'warning', 'danger' or ''
                                                     tooltip='Click to perform a quick Le Bail profile fitting '
                                                             '(not robust with multiple phases !)',
                                                     icon='play')
        box_fit = ipywidgets.VBox(layout=ipywidgets.Layout(border='solid', margin='3px 0px 3px 0px'))
        box_fit.children = [self.analysis_fit_dropdown, self.analysis_fit_button, self.crystal_vbox]

        # Pressure calculation widgets
        options = [f"S16-{k}" for k in load_dd07().keys()]
        options += [f"DD07-{k}" for k in load_dd07().keys()]
        options += ['BN', 'Re']
        self.pressure_button = ipywidgets.Button(description='Calc Pressure',
                                                 disabled=False, layout=ipywidgets.Layout(width="auto"),
                                                 button_style='',  # 'success', 'info', 'warning', 'danger' or ''
                                                 tooltip='Compute the pressure after selecting the method '
                                                         'and the fitted crystalline phase')
        self.pressure_method = ipywidgets.Dropdown(options=options, value=options[0],
                                                   description='Method',
                                                   tooltip='Select the reference method and crystalline phase'
                                                           'to compute the Pressure.')
        self.pressure_phase = ipywidgets.Dropdown(options=[], description='Crystal:',
                                                  tooltip='Select fitted Crystalline phase')
        self.pressure_temp = ipywidgets.BoundedFloatText(value=273, description='T(K)',
                                                         min=10, max=5000, step=1)
        self.pressure_out = ipywidgets.Text(value='', description='P(GPa):')

        with plt.ioff():
            # To plot pressure vs frame number
            self.plot_pressure_fig = plt.figure(figsize=(4, 3))
            self.plot_pressure_obj = None
            self.plot_pressure_fig.canvas.header_visible = False
            self.plot_pressure_fig.tight_layout()

        box_pressure = ipywidgets.VBox(layout=ipywidgets.Layout(border='solid', margin='3px 0px 3px 0px'))
        box_pressure.children = [self.pressure_button, self.pressure_method, self.pressure_phase,
                                 self.pressure_temp, self.pressure_out, self.plot_pressure_fig.canvas]

        self.analysis_menu.children = [box_fit, box_pressure]

        box0.children = [analysis_box, self.analysis_menu]
        # Update list of phases when changing tabs (not very elegant..)
        self.tabs.observe(self.on_change_tab)

        self.analysis_fit_button.on_click(self.on_analysis_fit)
        self.pressure_button.on_click(self.on_analysis_pressure)
        self.pressure_method.observe(self.on_analysis_pressure, 'value')
        self.pressure_temp.observe(self.on_analysis_pressure, 'value')
        return box0

    def make_tab_config(self, poni_filename=None, data_src=None, detector_geometry_filename=None,
                        processed_data_filename=None, processed_data_scan=1):
        box0 = ipywidgets.VBox()
        # Processed data
        label_processed = ipywidgets.Label(value="Input from PROCESSED_DATA (ewoksxrpd/pyFAI pipeline):")
        self.config_processed_data_filename = \
            ipywidgets.Text(value=processed_data_filename,
                            placeholder='/path/to/PROCESSED_DATA/../*.h5',
                            description='Processed data file:',
                            tooltip="Processed data hdf5 file from the ewoks-xrpd(pyFAI) "
                                    "pipeline. If this is given, the poni/detector distortion/"
                                    "data src entries are not needed",
                            disabled=False, continuous_update=False,
                            layout=ipywidgets.Layout(width="auto"))
        self.config_processed_scan = \
            ipywidgets.Dropdown(value=None,
                                options=[],
                                description='Scan #',
                                description_tooltip="Choose the scan # - the number of frames is indicated in []",
                                disabled=False)
        # Icon does not seem to work? Wrong auto-layout width ?
        config_processed_scan_refresh = ipywidgets.Button(description='Refresh scan list',
                                                          disabled=False, layout=ipywidgets.Layout(width="auto"),
                                                          button_style='',
                                                          # 'success', 'info', 'warning', 'danger' or ''
                                                          icon='rotate')

        vbox1 = ipywidgets.VBox([label_processed, self.config_processed_data_filename,
                                 ipywidgets.HBox([self.config_processed_scan, config_processed_scan_refresh])],
                                layout=ipywidgets.Layout(width="auto", border='solid', margin='2px'))
        # poni
        label_edf = ipywidgets.Label(value="Input from edf files:")

        self.config_poni_file = ipywidgets.Text(value=poni_filename,
                                                placeholder='/path/to/calibration.poni',
                                                description='PONI file:',
                                                tooltip="PONI file (only if loading from edf file "
                                                        "and not using the ewoks/pyFAI processed data)",
                                                disabled=False, layout=ipywidgets.Layout(width="auto"))
        # Detector geometry/distortion file
        self.config_detector_file = ipywidgets.Text(value=detector_geometry_filename,
                                                    placeholder='/path/to/Pilatus_ID06_final.h5 [optional]',
                                                    description='Detector geometry:',
                                                    tooltip="Detector geometry (optional, only if loading from "
                                                            "edf file and not using the ewoks/pyFAI processed data)",
                                                    disabled=False, layout=ipywidgets.Layout(width="auto"))
        # data src
        self.config_data_src = ipywidgets.Text(value=data_src,
                                               placeholder='/path/to/data/data%04d.edf',
                                               description='data src:',
                                               disabled=False, layout=ipywidgets.Layout(width="auto"),
                                               tooltip="File pattern for the edf files, e.g. /path/to/"
                                                       "data/data%04d.edf (only if loading from edf file and"
                                                       " not using the ewoks/pyFAI processed data)")
        vbox2 = ipywidgets.VBox([label_edf, self.config_poni_file, self.config_detector_file, self.config_data_src],
                                layout=ipywidgets.Layout(width="auto", border='solid', margin='2px'))

        # Crystal structures
        self.config_crystals = ipywidgets.Textarea(value=None, placeholder='CIF path or url (one per line)',
                                                   description='CIFs:',
                                                   tooltip="List (one per line) of crystal CIFs, either "
                                                           "an URL e.g. http://crystallography.net/cod/9000493.cif,"
                                                           "or /path/to/data.cif",
                                                   disabled=False, layout=ipywidgets.Layout(width="auto"))
        # Logging level
        self.config_logging = ipywidgets.Dropdown(options=['debug', 'info', 'warning', 'error', 'critical'],
                                                  value='info',
                                                  description='Logging level', disabled=False)
        self.config_logging.observe(self.on_config_logging)
        # Activate configuration
        self.config_button = ipywidgets.Button(description='Load configuration',
                                               disabled=False, layout=ipywidgets.Layout(width="auto"),
                                               button_style='',  # 'success', 'info', 'warning', 'danger' or ''
                                               tooltip='Click to activate the configuration & start loading data',
                                               icon='play')

        box0.children = [vbox1, vbox2, self.config_crystals, self.config_button, self.config_logging]

        self.load_config()
        self.config_processed_data_filename.observe(self.on_change_processed_data_filename, names='value')
        self.on_change_processed_data_filename()
        self.config_button.on_click(self.on_load_config)
        config_processed_scan_refresh.on_click(self.on_refresh_processed_available_scans)
        return box0

    def scan_data_folder(self):
        """Scan data folder to see how many frames are available.
        If processed data is used, just return the list of frames already
        integrated.

        Return: the list of available frames (should be alist from 0 to n_frames-1)
        """
        self.logger.debug(f"Opening: {self.processed_data_filename}")
        if self.processed_data_filename is not None:
            s = f"{self.processed_data_scan}.1/instrument/p900kw/data"
            data_raw_filename = self.processed_data_filename.replace("PROCESSED_DATA", "RAW_DATA")
            with dynamic_hdf5.File(data_raw_filename, retry_timeout=0, lima_names=["p900kw"]) as h:
                try:
                    # TODO: do we need to try again ?
                    self.logger.debug(f"Opening: {s} using blissdata.h5api.dynamic_hdf5")
                    nb = len(h[s])
                    v_i = range(nb)
                    self.logger.debug(f"scan_data_folder -> nb frames={nb}, shape={h[s].shape}")
                    for i in range(max(0, nb - 10), nb):
                        # For some reason blissdata reports a larger size than what is available,
                        # so check what is really accessible
                        try:
                            # test access
                            junk = h[s][i, 0, 0]
                            # self.logger.debug(f"Opening OK     {data_raw_filename}:{s}[{i}]")
                        except Exception as ex:
                            self.logger.debug(f"Failed to open {data_raw_filename}:{s}[{i}] "
                                              f"- last available frame {i - 1}")
                            v_i = v_i[:i]
                            break
                            # self.logger.error(traceback.format_exc())
                except Exception:
                    self.logger.error(f"Failed to open dataset: {self.processed_data_filename}::{s}")
                    raise RuntimeError("Failed to scan data - try again later or check the data")
        else:
            # Extract glob pattern
            pre, post = self.data_src.split('%')
            post = post[post.find('d') + 1:]
            pat = pre + "*" + post
            v_file = glob.glob(pat)
            v_file.sort()
            # Make sure files really match the pattern
            # remove the path (avoids issues with regex and paths with \ on Windows
            pre = os.path.split(pre)[1]
            v_i = []
            for s in v_file:
                try:
                    i = int(re.search("%s(.+?)%s" % (pre, post), os.path.split(s)[1]).group(1))
                    if os.path.split(self.data_src % i)[1] == os.path.split(s)[1]:
                        v_i.append(i)
                    else:
                        self.logger.debug("Could not match file: %s" % s)
                except Exception:
                    self.logger.debug("Could not match file: %s (failed to read i)" % s)
            # try:
            #    v_i = [int(re.search("%s(.+?)%s" %(pre, post), s).group(1)) for s in v_file]
            # except Exception:
            #    v_i = np.arange(len(v_file), dtype=np.int32)
            # DEBUG/DEVELOPMENT
        if self._ct_live >= 0:
            m = 40 + self._ct_live
            self._ct_live += np.random.randint(1, 5 + 1)  # simulate a few new frames each time
            if len(v_i) > m:
                v_i = v_i[:m]
        with NoEventContextManager(self):
            self.slider_frame.max = v_i[-1]
        return v_i

    def load_all_available(self):
        v_i = self.scan_data_folder()
        self.logger.info("Loading all %d frames" % len(v_i))
        self.progress_bar.max = len(v_i)

        if self.iobs2d is None:
            self.iobs2d = np.zeros((len(v_i), len(self.tth)), dtype=np.float32)
            self.iobs2d_sigma = np.zeros_like(self.iobs2d)
            self.logger.debug(f"Init iobs2d.. shape={self.iobs2d.shape} DONE")
        elif len(self.iobs2d) <= len(v_i):
            self.logger.debug(f"Expanding Iobs2d")
            self.iobs2d = np.pad(self.iobs2d, pad_width=((0, len(v_i) - len(self.iobs2d)), (0, 0)), mode='constant',
                                 constant_values=0)
            self.iobs2d_sigma = np.pad(self.iobs2d_sigma,
                                       pad_width=((0, len(v_i) - len(self.iobs2d_sigma)), (0, 0)),
                                       mode='constant', constant_values=0)
        # ct = 0
        for i in v_i:
            self.logger.debug(f"loading frame #{i}")
            if i % 10 == 0:
                self.progress_bar.value = i  # Avoid too frequent updates
            if i not in self.v_i:
                try:
                    # if i == 3:
                    #     raise RuntimeError("Random error test")  # debugging data availability
                    self.load_iobs_raw_i(i)
                except Exception:
                    self.logger.error(f"Failed loading frame #{i} - stopping loading of available frames, "
                                      f"if 'live' is activated will try again shortly")
                    with NoEventContextManager(self):
                        self.logger.info(f"Setting number of frames to {len(self.v_i)}")
                        self.iobs2d = self.iobs2d[:len(self.v_i)]
                        self.iobs2d_sigma = self.iobs2d_sigma[:len(self.v_i)]
                        self.slider_frame.value = len(self.v_i)
                    return
                mask = np.logical_or((self.iobs_raw > 4e9) * (self.iobs_raw < 0), self.mask)
                tth, iobs, sigma = self.azi.integrate1d_ng(self.iobs_raw, self.iobs_raw.shape[-1], mask=mask,
                                                           unit="2th_deg", dummy=0, radial_range=self.radial_range,
                                                           error_model="poisson")

                self.iobs2d[i] = np.take(iobs, self.idx_nomask)
                self.iobs2d_sigma[i] = np.take(sigma, self.idx_nomask)

                self.v_i.append(i)
                if i % 50 == 0:
                    with NoEventContextManager(self):
                        self.slider_frame.value = i
                        # self.update_2d()  # does not work
            self.logger.debug(f"loading frame #{i}.. DONE")

        self.logger.info("Loading all %d frames.. done" % len(self.v_i))

    def load_iobs_raw_i(self, i=None):
        if i is None:
            i = self.slider_frame.value
        if self.processed_data_filename is None:
            self.logger.debug(
                "Loading: %s [dt=%4.2fs]" % (os.path.split(self.data_src)[1] % i, timeit.default_timer() - self.t0))
            self.iobs_raw = fabio.open(self.data_src % i).data
        else:
            # We use the blissdata dynamic API, which will find the data from chunk files
            # even if the scan is not finished, as long as we try to access it via an
            # [NXDetector]/data dataset
            # See https://bliss.gitlab-pages.esrf.fr/bliss/master/data/h5py_like_api.html#static-vs-dynamic
            s = f"{self.processed_data_scan}.1/instrument/p900kw/data"
            data_raw_filename = self.processed_data_filename.replace("PROCESSED_DATA", "RAW_DATA")
            # try:
            #     has_data_processed = s in h5py_utils.File(self.processed_data_filename, 'r', locking=False)
            #     has_data_processed = False
            # except Exception as ex:
            #     self.logger.error(f"Failed to open processed data file: {self.processed_data_filename}")
            #     raise ex
            try:
                has_data_raw = s in dynamic_hdf5.File(data_raw_filename, retry_timeout=0, lima_names=["p900kw"])
            except Exception as ex:
                self.logger.error(f"Failed to open raw data file: {self.processed_data_filename}")
                raise ex

            self.logger.debug(f"{s} in raw [{has_data_raw}]")
            if has_data_raw:
                src = data_raw_filename
            else:
                msg = f"Could not find RAW 2D data either from {self.processed_data_filename} or {data_raw_filename}"
                self.logger.error(msg)
                raise RuntimeError(msg)
            self.logger.debug(f"Loading 2D data: {src}::{s}[{i}]")
            with dynamic_hdf5.File(src, retry_timeout=0, lima_names=["p900kw"]) as h:
                try:
                    self.iobs_raw = h[s][i]
                except Exception as ex:
                    er = f"Failed opening the raw 2D data - 2D raw image #{i} not available yet ?"
                    self.logger.error(er)
                    raise RuntimeError(er)
                self.logger.debug(f"Loading 2D data: {self.processed_data_filename}::{s}[{i}]: OK")

    def load_i(self, update_plot=True):
        """Load frame #i, display it"""
        try:
            self.logger.debug("load_i")
            i = self.slider_frame.value
            try:
                self.load_iobs_raw_i(i)
            except Exception as ex:
                self.logger.error(f"Failed loading frame #{i} - if 'live' is activated will try again shortly")
                raise ex
            self.t0 = timeit.default_timer()

            if self.mask is None:
                self.logger.info("Generating mask for gaps")
                # Build Mask for gaps
                ny, nx = self.iobs_raw.shape
                self.mask = np.zeros((ny, nx), dtype=bool)
                for ii in range(1, 9):
                    # Mask an extra pixel before at low angle and an extra after at high - smaller surface / incidence issue ?
                    pre = -7  # if i >=3 else -8
                    post = 1  # if i <7 else 2
                    self.mask[:, ii * 494 + pre:ii * 494 + post] = True
                for ii in range(1, 10):
                    self.mask[:, ii * 494 - 251] = True

            if i not in self.v_i:
                mask = np.logical_or((self.iobs_raw > 4e9) * (self.iobs_raw < 0), self.mask)
                if self.radial_range is None:
                    self.logger.debug("First dummy azimuthal integration")
                    # First perform a dummy integration to get the min/max range
                    # Make sure no data is null so the automatic domain range is complete
                    res = self.azi.integrate1d_ng(np.ones_like(self.iobs_raw), self.iobs_raw.shape[-1], mask=None,
                                                  unit="2th_deg", dummy=None)
                    self.radial_range = res.radial[0], res.radial[-1]
                self.logger.debug(f"Azimuthal integration for raw data #{i}")
                res = self.azi.integrate1d_ng(self.iobs_raw, self.iobs_raw.shape[-1], mask=mask, unit="2th_deg",
                                              dummy=None,
                                              radial_range=self.radial_range, error_model="poisson")
                self.tth, iobs, sigma = res

                # Update normalisation range if needed (once)
                if self.range_normalisation.min < 0:
                    self.logger.debug("Update normalisation range")
                    with NoEventContextManager(self):
                        self.range_normalisation.max = self.tth.max()
                        self.range_normalisation.min = self.tth.min()
                        # Use the first degree as default range
                        self.range_normalisation.value = [self.tth.min(), self.tth.min() + 1]

                if True:
                    # Remove values around gaps (tth step will not be constant)
                    if self.idx_nomask is None:
                        self.idx_nomask = np.where(res.sum_normalization > 0.85 * res.sum_normalization.max())[0]
                    self.tth = np.take(self.tth, self.idx_nomask)
                    iobs = np.take(iobs, self.idx_nomask)
                    sigma = np.take(sigma, self.idx_nomask)

                self.v_i.append(i)
                if self.iobs2d is None:
                    self.iobs2d = np.zeros((i + 1, len(self.tth)), dtype=np.float32)
                    self.iobs2d_sigma = np.zeros_like(self.iobs2d)
                elif len(self.iobs2d) <= i:
                    self.iobs2d = np.pad(self.iobs2d, pad_width=((0, i - len(self.iobs2d) + 1), (0, 0)),
                                         mode='constant',
                                         constant_values=0)
                    self.iobs2d_sigma = np.pad(self.iobs2d_sigma,
                                               pad_width=((0, i - len(self.iobs2d_sigma) + 1), (0, 0)),
                                               mode='constant', constant_values=0)
                self.iobs2d[i] = iobs
                self.iobs2d_sigma[i] = sigma
                if update_plot:
                    self.update_plot_ranges()
                    self.update_2d()
            self.logger.debug(f"load_i: update plot ? {update_plot}")
            if update_plot:
                self.update_plot_ranges()
                self.update_raw()
                self.update_1d()
            # Update the powder pattern
            if self.powder_pattern is not None:
                self.powder_pattern.SetPowderPatternObs(self.iobs2d[self.slider_frame.value].astype(np.float64))
                # TODO: uncomment & set the name when pyobjcryst fix is in conda as well
                #  [https://github.com/diffpy/pyobjcryst/commit/51c079b43135fd0c9685b4b215a4a93aac1c2998]
                # self.powder_pattern.SetName(os.path.split(self.data_src)[1] % i)
                self.update_powder_pattern_plot()  # This won't update the plot until it's displayed
            self.logger.debug(f"load_i[{i}].. DONE")
        except Exception as ex:
            self.logger.error(traceback.format_exc())
            self.logger.error(f"load_i[{i}].. ERROR")

    def update_plot_ranges(self):
        with NoEventContextManager(self):
            self.logger.debug(f"update_plot_ranges")
            # Raw 2D data
            b0 = self.range_cmap_raw.min == self.range_cmap_raw.value[0]
            b1 = self.range_cmap_raw.max == self.range_cmap_raw.value[1]
            tmp = np.ma.masked_array(self.iobs_raw, np.logical_or(self.iobs_raw <= 0, self.mask))
            vmin, vmax = tmp.min(), tmp.max()
            vmin = np.log10(vmin) if vmin > 1 else 0
            vmax = np.log10(vmax) if vmax > 1 else 1
            v0, v1 = self.range_cmap_raw.value
            try:
                # 2-step update so that no value is outside min/max range
                self.range_cmap_raw.min = min(vmin, v0, self.range_cmap_raw.min)
                self.range_cmap_raw.max = max(vmax, v1, self.range_cmap_raw.max)
                self.range_cmap_raw.value = [(self.range_cmap_raw.min + self.range_cmap_raw.max)] * 2
                self.range_cmap_raw.min = vmin
                self.range_cmap_raw.max = vmax
                self.range_cmap_raw.value = vmin if b0 else max(min(v0, vmax), vmin), \
                    vmax if b0 else min(max(v1, vmin), vmax)
            except Exception as ex:
                self.logger.error(traceback.format_exc())

            # 2D data
            b1 = self.range_cmap_2d.max == self.range_cmap_2d.value[1]
            b0 = self.range_cmap_2d.min == self.range_cmap_2d.value[0]
            vmin, vmax = self.iobs2d.min(), self.iobs2d.max()
            vmin = np.log10(vmin) if vmin > 1 else 0
            vmax = np.log10(vmax) if vmax > 1 else 1
            v0, v1 = self.range_cmap_2d.value
            try:
                # 2-step update so that no value is outside min/max range
                self.range_cmap_2d.min = min(vmin, v0, self.range_cmap_2d.min)
                self.range_cmap_2d.max = max(vmax, v1, self.range_cmap_2d.max)
                self.range_cmap_2d.value = [(self.range_cmap_2d.min + self.range_cmap_2d.max) / 2] * 2
                self.range_cmap_2d.min = vmin
                self.range_cmap_2d.max = vmax
                self.range_cmap_2d.value = vmin if b0 else max(min(v0, vmax), vmin), \
                    vmax if b0 else min(max(v1, vmin), vmax)
            except Exception as ex:
                self.logger.error(traceback.format_exc())
                self.logger.debug(f"update_plot_ranges: vmin={vmin} v0={v0} "
                                  f"self.range_cmap_2d.min={self.range_cmap_2d.min}  "
                                  f"vmax={vmax} v1={v1} self.range_cmap_2d.max={self.range_cmap_2d.max}")

    def _tth2d(self, tth):
        # Convert 2theta to d-spacing (in Angstroems)
        return self.azi.get_wavelength() * 1e10 / (2 * np.sin(np.deg2rad(tth / 2)))

    def _d2tth(self, d):
        # Convert 2theta to degrees
        return 2 * np.rad2deg(self.azi.get_wavelength() * 1e10 / (2 * d))

    def _plot1d_format_coord(self, x, y):
        return f"2\u03B8={x:.3f}° d={self._tth2d(x):.3f}\u212B y={y:10.1f}"

    def update_1d(self):
        self.logger.debug(f"update_1d: iobs2d is not None ? {self.iobs2d is not None}")
        if self.iobs2d is not None:
            i = self.slider_frame.value
            self.logger.debug("Updating 1D plot with frame #%d" % i)
            with plt.ioff():
                if self.plot_1d_obj is None:
                    self.plot_1d_obj = self.plot_1d_ax.plot(self.tth, self.iobs2d[i], color='black', linewidth=1)[0]
                    self.plot_1d_ax.format_coord = self._plot1d_format_coord
                    # Setup secondary axis with d-spacing
                    secax = self.plot_1d_ax.secondary_xaxis('top', functions=(self._tth2d, self._d2tth))
                    secax.set_xlabel('d [\u212B]')
                    # d = self.tth2d(self.tth)
                    # secax.xaxis.set_major_locator(ticker.FixedLocator(d[::10], nbins=10))
                    # secax.xaxis.set_minor_locator(ticker.FixedLocator(d[::10], nbins=40))
                    secax.xaxis.set_major_locator(ticker.MaxNLocator(nbins=20))
                    secax.xaxis.set_minor_locator(ticker.MaxNLocator(nbins=40))
                    plt.setp(secax.xaxis.get_majorticklabels(), rotation=45)

                else:
                    self.plot_1d_obj.set_ydata(self.iobs2d[i])
                if self.toggle_autoscale_1d.value:
                    x0, x1 = self.plot_1d_ax.get_xlim()
                    vx = self.plot_1d_obj.get_xdata()
                    ix = np.where((vx >= x0) * (vx <= x1))
                    v = np.take(self.iobs2d[i], ix)
                    v = np.ma.masked_array(v, v <= 0)  # Mask gaps with iobs=0
                    self.plot_1d_ax.set_ylim(v.min(), v.max())
                sc = self.choice_1d_yscale.value
                if sc == "sqrt":
                    sc = "function"
                if self.plot_1d_ax.get_yscale() != sc:
                    self.logger.info("Setting 1D Y scale to: %s (old: %s)" % (sc, self.plot_1d_ax.get_yscale()))
                    if sc == "function":
                        self.plot_1d_ax.set_yscale("function", functions=(np.sqrt, np.square))
                    else:
                        self.plot_1d_ax.set_yscale(sc)

                # plt.axis('on')

                # Plot reflection locations
                for ic in range(len(self.crystal_phases)):
                    if self.crystal_phases[ic]['toggle'].value:
                        # find the tth positions of reflections
                        pd = self.crystal_phases[ic]['diffraction']
                        tth = 2 * np.rad2deg(pd.GetTheta())  # reflections 2theta coordinates
                        x0, x1 = self.plot_1d_ax.get_xlim()
                        y0, y1 = self.plot_1d_ax.get_ylim()
                        dy = (y1 - y0) / 40 * (ic + 1)  # markers above observed plot
                        vx, vix, vy = [], [], []
                        ixmax = len(self.tth) - 1
                        for x in tth:
                            if x0 <= x <= x1:
                                ix = np.argmin(abs(x - self.tth))
                                vx.append(x)
                                vix.append(ix)
                                # Take into account nearby points to choose y height
                                tmp = self.iobs2d[i, max(0, ix - 1):min(ixmax, ix + 2)].max()
                                vy.append(min(tmp + dy, y1))  # don't go beyond ymax
                        # vy= np.take(self.iobs2d[i], vix) + dy
                        if len(vx):
                            c = self.crystal_phases[ic]['widget'].color_picker.value
                            n = self.crystal_phases[ic]['crystal'].GetName()
                            if self.crystal_phases[ic]['scatter'] is None:
                                self.crystal_phases[ic]['scatter'] = \
                                    self.plot_1d_ax.scatter(vx, vy, s=20, alpha=1, color=c,
                                                            marker='|', linewidths=2, clip_on=False)
                                self.crystal_phases[ic]['text'] = \
                                    self.plot_1d_ax.text(0.99, 0.99 - ic * 0.05, n, horizontalalignment='right',
                                                         verticalalignment='top', color=c,
                                                         transform=self.plot_1d_ax.transAxes)

                            else:
                                self.crystal_phases[ic]['scatter'].set_alpha(1)
                                self.crystal_phases[ic]['text'].set_alpha(1)
                                self.crystal_phases[ic]['scatter'].set_offsets(np.column_stack((vx, vy)))
                                self.crystal_phases[ic]['scatter'].set_color(c)
                                self.crystal_phases[ic]['text'].set_text(n)
                    else:
                        if self.crystal_phases[ic]['scatter'] is not None:
                            self.crystal_phases[ic]['scatter'].set_alpha(0)
                            self.crystal_phases[ic]['text'].set_alpha(0)
                self.plot_1d_fig.canvas.draw()
                self.plot_1d_fig.canvas.flush_events()

    def update_raw(self):
        if self.iobs_raw is not None:
            self.logger.debug(f"Updating raw 2D plot with frame #{self.slider_frame.value}")
            with plt.ioff():
                if self.plot_2draw_obj is None:
                    self.plot_2draw_obj = \
                        self.plot_2draw_ax.imshow(self.iobs_raw,
                                                  cmap=self.choice_cmap.value,
                                                  norm=LogNorm(vmin=10 ** self.range_cmap_raw.value[0],
                                                               vmax=10 ** self.range_cmap_raw.value[1]))
                    plt.axis('off')
                else:
                    self.plot_2draw_obj.set_data(self.iobs_raw)
                    if self.choice_cmap.value != self.plot_2draw_obj.get_cmap().name:
                        self.plot_2draw_obj.set_cmap(self.choice_cmap.value)
                    if not np.isclose(np.log10(self.plot_2draw_obj.norm.vmin), self.range_cmap_raw.value[0]) or \
                            not np.isclose(np.log10(self.plot_2draw_obj.norm.vmax), self.range_cmap_raw.value[1]):
                        self.plot_2draw_obj.set_norm(LogNorm(vmin=10 ** self.range_cmap_raw.value[0],
                                                             vmax=10 ** self.range_cmap_raw.value[1]))
                    self.logger.debug("raw range: %6.3f-%6.3f" % (10 ** self.range_cmap_raw.value[0],
                                                                  10 ** self.range_cmap_raw.value[1]))
                # self.plot_2draw_fig.canvas.header_visible = False
                # self.plot_2draw_fig.canvas.footer_visible = False
                self.plot_2draw_fig.canvas.draw()
                self.plot_2draw_fig.canvas.flush_events()

    def get_iobs2d(self):
        """Get 2D iobs, normalised or not according to normalisation range slider"""
        iobs2d = self.iobs2d

        # normalise iobs2d ?
        tth0, tth1 = self.range_normalisation.value
        if tth1 > tth0:
            idx0 = np.argmin(abs(self.tth - tth0))
            idx1 = np.argmin(abs(self.tth - tth1))
            if idx1 > idx0:
                norm = iobs2d[:, idx0:idx1].mean(axis=1)
                norm = (norm / norm.mean())
                # Handle empty frames
                tmp = norm > 0.05
                norm = norm * tmp + 1 * (1 - tmp)
                iobs2d = iobs2d / norm[:, np.newaxis]
        return iobs2d

    def update_2d(self):
        if self.iobs2d is not None:
            self.logger.debug("Updating 2D plot")
            old_x_lim, old_y_lim = None, None
            with plt.ioff():
                h, v0 = self.plot_2d_fig.get_size_inches()
                iobs2d = self.get_iobs2d()
                v = min(max(2., len(iobs2d) / 100), 8.)

                # Only change the figure size by 1/4 inch increments
                v = np.round(v * 4) / 4
                if not np.isclose(v, v0, atol=0.01):
                    self.plot_2d_fig.set_size_inches(h, v)
                if self.plot_2d_obj is None:
                    self.plot_2d_obj = \
                        self.plot_2d_ax.imshow(iobs2d, cmap=self.choice_cmap.value,
                                               norm=LogNorm(vmin=10 ** self.range_cmap_2d.value[0],
                                                            vmax=10 ** self.range_cmap_2d.value[1]),
                                               extent=(self.tth[0], self.tth[-1], 0,
                                                       len(self.iobs2d)),
                                               aspect='auto', origin='lower')
                    # plt.tight_layout()
                else:
                    # Record original zoom
                    ax = self.plot_2d_fig.get_axes()[0]
                    old_x_lim = ax.get_xlim()
                    if np.allclose(old_x_lim, [self.tth[0], self.tth[-1]]):
                        old_x_lim = None  # No zoom
                    old_y_lim = ax.get_ylim()
                    if np.allclose(old_y_lim, [0, self._last_iobs2d_len]):
                        old_y_lim = None  # No zoom

                    # Update data
                    self.plot_2d_obj.set_data(iobs2d)
                    self.plot_2d_obj.set_extent((self.tth[0], self.tth[-1], 0, len(self.iobs2d)))
                    if self.choice_cmap.value != self.plot_2d_obj.get_cmap().name:
                        self.plot_2d_obj.set_cmap(self.choice_cmap.value)
                    if not np.isclose(np.log10(self.plot_2d_obj.norm.vmin), self.range_cmap_2d.value[0]) or \
                            not np.isclose(np.log10(self.plot_2d_obj.norm.vmax), self.range_cmap_2d.value[1]):
                        self.plot_2d_obj.set_norm(LogNorm(vmin=10 ** self.range_cmap_2d.value[0],
                                                          vmax=10 ** self.range_cmap_2d.value[1]))
                        self.logger.info("2D range: %6.3f-%6.3f" % (10 ** self.range_cmap_2d.value[0],
                                                                    10 ** self.range_cmap_2d.value[1]))
                    if old_y_lim is not None or old_x_lim is not None:
                        # Update data limits and zoom back to user-selected zoom, if any
                        ax.relim()
                        ax.autoscale()
                        self.plot_2d_fig.canvas.manager.toolbar.update()  # Clear the axes stack
                        self.plot_2d_fig.canvas.manager.toolbar.push_current()  # New home
                        if old_x_lim is not None:
                            ax.set_xlim(old_x_lim)
                        if old_y_lim is not None:
                            ax.set_ylim(old_y_lim)

                if not np.isclose(v, v0, atol=0.01):
                    self.plot_2d_fig.tight_layout(pad=2.5)
                self.plot_2d_fig.canvas.draw()
                self.plot_2d_fig.canvas.flush_events()
                # 'hot', 'gnuplot2' 'jet'
                # plt.colorbar()

                self._last_iobs2d_len = len(iobs2d)
            self.logger.debug("Updating 2D plot.. DONE")

    def update_powder_pattern_plot(self):
        """Update powder pattern plot when observed pattern or lattice parameters have changed"""
        try:
            if self.tabs.selected_index == 1 and self.analysis_fig is not None:
                self.logger.debug("Updating analysis powder pattern plot")
                with plt.ioff():
                    # Record previous limits (to avoid being stuck on zoomed axes)
                    ax = self.analysis_fig.get_axes()[0]
                    if len(ax.findobj(Legend)) == 0:
                        # Exclude the initial plot before there is a legend
                        old_x_lim, old_y_lim = None, None
                    else:
                        old_x_lim = ax.get_xlim()
                        if np.allclose(old_x_lim, [self.tth[0], self.tth[-1]]):
                            old_x_lim = None  # No zoom
                        old_y_lim = ax.get_ylim()
                        if np.allclose(old_y_lim, [0, self.iobs2d[self.slider_frame.value].max()]):
                            old_y_lim = None  # No zoom

                    self.powderpattern_background.UnFixAllPar()
                    self.powderpattern_background.OptimizeBayesianBackground()
                    self.powder_pattern.FitScaleFactorForIntegratedRw()
                    # Update colours
                    ic = 0
                    for i in range(min(len(self.crystal_phases), len(self.powder_pattern._colour_phases))):
                        wc = self.crystal_phases[i]
                        if wc['toggle'].value:
                            self.powder_pattern._colour_phases[ic] = wc['widget'].color_picker.value
                            ic += 1
                    self.powder_pattern.plot(fig=self.analysis_fig, hkl=True)

                    # Update data limits and zoom back to user-selected zoom, if any
                    if old_y_lim is not None or old_x_lim is not None:
                        ax = self.analysis_fig.get_axes()[0]  # Changed ?
                        ax.relim()
                        ax.autoscale()
                        ax.autoscale_view()
                        ax.set_ylim(0, self.iobs2d[self.slider_frame.value].max())
                        self.analysis_fig.canvas.manager.toolbar.update()  # Clear the axes stack
                        self.analysis_fig.canvas.manager.toolbar.push_current()  # New home
                        if old_x_lim is not None:
                            ax.set_xlim(old_x_lim)
                        if old_y_lim is not None:
                            ax.set_ylim(old_y_lim)

                    # Set format to print coordinates in footer when hovering with the pointer
                    self.analysis_fig.axes[0].format_coord = self._plot1d_format_coord  # See update_1d()
        except Exception as ex:
            self.logger.error(traceback.format_exc())

    def on_change_processed_data_filename(self, b=None):
        if self.config_processed_data_filename.value is None:
            return
        try:
            self.refresh_processed_available_scans()
        except Exception as ex:
            # An exception can be raised if the file is not yet ready - ignore it when
            # the processed file has just been changed
            self.logger.info("An error occured trying to open the processed data - this is OK if "
                             "processing has just begun (need data to be flushed)\n"
                             + traceback.format_exc())
            pass

    def on_refresh_processed_available_scans(self, b=None):
        self.refresh_processed_available_scans()
        v = np.array([n[1] for n in self.config_processed_scan.options], dtype=int)
        self.logger.info(f"{len(v)} scans available, newest=#{v[0]}")

    def refresh_processed_available_scans(self):
        """
        Open the processed data file and see which scans are available, and
        with how many frames
        :return: nothing. Update the list of available scans
        """
        if os.path.exists(self.config_processed_data_filename.value):
            try:
                opt = []
                with h5py_utils.File(self.config_processed_data_filename.value, 'r', locking=False) as h:
                    for k in h.keys():
                        # TODO: do we need to handle scans not in the form of 'NN.1' ?
                        n = int(k.split('.')[0])  # Scan number
                        # nb = len(h[f"{n}.1/measurement/p900kw"])  # Number of available frames
                        nb = len(h[f"{n}.1/p900kw_integrate/integrated/intensity"])  # Number of available frames
                        if nb > 15:
                            opt.append((f"{n:3d}[{nb}]", n))
                opt.sort(key=lambda x: -x[1])
                self.config_processed_scan.options = opt
                if self.config_processed_scan.value is None:
                    self.config_processed_scan.value = opt[0][1]
            except Exception:
                # self.logger.debug(traceback.format_exc())
                er = f"Failed querying the available scans - " \
                     f"if this repeats check if this really is a processed data file ? "
                self.logger.error(er)
                # raise RuntimeError(er)
        else:
            self.logger.error(f"Processed data filename does not exist: {self.config_processed_data_filename.value}")

    def on_load_config(self, b):
        self.logger.info("Loading configuration")

        # Only re-init data if configuration has changed. Otherwise, only re-init the powder pattern
        if self.data_src != self.config_data_src.value or self.poni_filename != self.config_poni_file.value \
                or self.config_detector_file.value != self.detector_geometry_filename \
                or self.config_processed_data_filename.value != self.processed_data_filename \
                or self.config_processed_scan.value != self.processed_data_scan:
            # keep old values, in case config goes wrong
            data_src0 = self.data_src
            poni_filename0 = self.poni_filename
            detector_geometry_filename0 = self.detector_geometry_filename
            processed_data_filename0 = self.processed_data_filename
            processed_scan0 = self.processed_data_scan

            if len(self.config_processed_data_filename.value):
                self.logger.info("Loading configuration from processed data")
                if not os.path.exists(self.config_processed_data_filename.value):
                    self.logger.error(f"The processed data file "
                                      f"[{self.config_processed_data_filename.value}] was not found")
                    return
                try:
                    with h5py_utils.File(self.config_processed_data_filename.value, 'r', locking=False) as h:
                        s = f"{self.config_processed_scan.value}.1/p900kw_integrate/integrated"
                        if s not in h:
                            self.logger.error(f"Failed opening the processed dataset "
                                              f"[{self.config_processed_data_filename.value}::{s}]")
                            return
                except Exception:
                    self.logger.error(f"Failed opening the processed data file "
                                      f"[{self.config_processed_data_filename.value}]")
                    return
                self.processed_data_filename = self.config_processed_data_filename.value
                self.processed_data_scan = self.config_processed_scan.value
            else:
                self.logger.info("Loading configuration: trying edf+poni file configuration")
                if self.config_poni_file.value is None:
                    self.logger.error("You must enter a poni filename first")
                    return

                if len(self.config_poni_file.value) == 0:
                    self.logger.error("You must enter a poni filename first")
                    return

                if not os.path.exists(self.config_poni_file.value):
                    self.logger.error("The poni filename (%s) was not found" % self.config_poni_file.value)
                    return

                self.poni_filename = self.config_poni_file.value

                self.detector_geometry_filename = None
                if self.config_detector_file.value is not None:
                    if len(self.config_detector_file.value) > 3:
                        if not os.path.exists(self.config_detector_file.value):
                            self.logger.error("The detector geometry filename (%s) was not found" %
                                              self.config_detector_file.value)
                            return

                        self.detector_geometry_filename = self.config_detector_file.value

                if self.config_data_src.value is None:
                    self.logger.error("You must enter the file pattern for the data files first")
                    return

                if len(self.config_data_src.value) == 0:
                    self.logger.error("You must enter the file pattern for the data files first")
                    return

                if not os.path.exists(self.config_data_src.value % 0):
                    self.logger.error("The first data file (%s) was not found" % (self.config_data_src.value % 0))
                    return

                self.data_src = self.config_data_src.value
            self.logger.info("New or changed configuration - initialising all data arrays")
            try:
                self.init_data()
            except Exception as ex:
                self.logger.error(traceback.format_exc())
                self.logger.error("Failed to init data- files may not be ready yet, try again in a moment..")
                # Restore initial values
                self.data_src = data_src0
                self.poni_filename = poni_filename0
                self.detector_geometry_filename = detector_geometry_filename0
                self.processed_data_filename = processed_data_filename0
                self.processed_data_scan = processed_scan0
                return
        else:
            self.logger.info("Unchanged configuration - keeping all data arrays, resetting powder pattern")

        self.init_power_pattern()
        # load crystals
        for cif in self.config_crystals.value.split('\n'):
            if len(cif) >= 3:
                if cif.lower() == 'custom':
                    c = Crystal()
                    c.SetName("Custom")
                    self.add_crystal(c)
                else:
                    self.add_crystal_cif(cif)
        # Save configuration as a json file, so it can be auto-magically re-used
        self.save_config()
        # display live tab
        if self.tab_live not in self.tabs.children:
            self.tabs.children = self.tab_live, self.tab_analysis, self.tab_config
            self.tabs.set_title(0, "Live integration")
            self.tabs.set_title(1, "Analysis")
            self.tabs.set_title(2, "Configuration")
        self.tabs.selected_index = 0

    def on_config_logging(self, v=None):
        if v is not None and self._enable_events:
            if v['name'] != 'value':
                return
            lev = {'debug': logging.DEBUG, 'info': logging.INFO, 'warning': logging.WARNING,
                   'error': logging.ERROR, 'critical': logging.CRITICAL}[self.config_logging.value]
            self.logger.setLevel(lev)

    def on_slider_frame(self, v=None):
        """
        Load frame #i
        """
        if v is not None and self._enable_events:
            if v['name'] != 'value':
                return
            if self.slider_frame.value != self.slider_frame.max:
                # pass
                self.toggle_live.value = False
            self.load_i(update_plot=True)

    def on_live(self, v=None):
        """Enable live view"""
        if v is not None and self._enable_events:
            if v['name'] != 'value':
                return
            if self.toggle_live.value:
                self.logger.info("Enabling live view")
                self.load_all_available()
                self.update_2d()
                self.run_live()
            else:
                self.logger.info("Disabling live view")

    def on_autoscale_1d(self, v=None):
        if v is not None and self._enable_events:
            if v['name'] != 'value':
                return
            if self.toggle_autoscale_1d.value:
                self.update_1d()

    def on_autoscale_2d(self, v=None):
        if v is not None and self._enable_events:
            if v['name'] != 'value':
                return
            if self.toggle_autoscale_2d.value:
                self.update_raw()

    def on_range_norm(self, v=None):
        if v is not None and self._enable_events:
            if v['name'] != 'value':
                return
            self.update_2d()

    def on_change_colors(self, v=None):
        # self.logger.debug(f"on_change_colors ?")
        # self.logger.debug(f"on_change_colors ? {v is not None} {self._enable_events}")
        if v is not None and self._enable_events:
            if v['name'] != 'value':
                return
            self.update_2d()
            self.update_raw()

    def on_change_1d_scale(self, v=None):
        if v is not None and self._enable_events:
            if v['name'] != 'value':
                return
            self.update_1d()

    def on_click_2d(self, event):
        """Select a line in the 2D plot"""
        # self.logger.info('%s click: button=%d, x=%d, y=%d, xdata=%f, ydata=%f' %
        #                 ('double' if event.dblclick else 'single', event.button,
        #                  event.x, event.y, event.xdata, event.ydata))
        if event.button == 3:  # right-click
            x, y, xdata, ydata = event.x, event.y, event.xdata, event.ydata
            ydata = int(ydata)
            if self.toggle_live.value:
                self.toggle_live.value = False
                self.logger.info("Selected line #%d - disabling live update" % ydata)
            else:
                self.logger.info("Selected line #%d" % ydata)
            self.slider_frame.value = ydata  # Will automatically trigger update

    def on_toggle_crystal(self, v):
        if v is not None and self._enable_events:
            if v['name'] == 'value':
                self._update_powder_pattern_phases()
                self.update_1d()
                self.update_powder_pattern_plot()
                if self.tabs.selected_index == 1:
                    opt = ['all phases']
                    for i in range(len(self.crystal_phases)):
                        wc = self.crystal_phases[i]
                        if wc['toggle'].value:
                            c = wc["crystal"]
                            opt.append(f"{i}-{c.GetName()}[{c.GetFormula()}]")
                    self.analysis_fit_dropdown.options = opt
                    self.pressure_phase.options = opt[1:]

    def on_change_crystal_tuning(self, v):
        if v is not None:
            if v['name'] != 'value':
                return
        self.apply_crystal_tuning()

    def apply_crystal_tuning(self):
        if self.crystal_tuning_toggle.value:
            step = 0.001
        else:
            step = 0.01
        for c in self.crystal_phases:
            w = c['widget']
            w.lattice_a.step = step
            w.lattice_b.step = step
            w.lattice_c.step = step
            w.lattice_alpha.step = step * 100
            w.lattice_beta.step = step * 100
            w.lattice_gamma.step = step * 100

    def on_change_tab(self, v):
        """Update the fit dropdown menu when crystal phases are added/removed"""
        if v is not None and self._enable_events:
            if v['name'] != 'selected_index':
                return
        if self.tabs.selected_index == 1:
            opt = ['all phases']
            for i in range(len(self.crystal_phases)):
                wc = self.crystal_phases[i]
                if wc['toggle'].value:
                    c = wc["crystal"]
                    opt.append(f"{i}-{c.GetName()}[{c.GetFormula()}]")
            self.analysis_fit_dropdown.options = opt
            self.pressure_phase.options = opt[1:]
            self.update_powder_pattern_plot()

    def on_analysis_fit(self, b):
        try:
            self.logger.info("Profile fitting...")
            self.powderpattern_background.UnFixAllPar()
            self.powderpattern_background.OptimizeBayesianBackground()
            self.powder_pattern.FitScaleFactorForIntegratedRw()
            with plt.ioff():
                self.powder_pattern.plot(hkl=True, fig=self.analysis_fig)
            if self.analysis_fit_dropdown.value == "all phases":
                vdiff = []
                for wc in self.crystal_phases:
                    if wc['toggle'].value:
                        vdiff.append(wc['diffraction'])
            else:
                i = int(self.analysis_fit_dropdown.value.split("-")[0])
                vdiff = [self.crystal_phases[i]['diffraction']]

            for pdiff in vdiff:
                self.logger.info("Profile fitting for phase: %s" % pdiff.GetCrystal().GetName())
                self.powder_pattern.quick_fit_profile(pdiff=pdiff, init_profile=True, plot=False, backgd=False,
                                                      constant_width=True, width=False, eta=True, cell=True,
                                                      verbose=False)
                with plt.ioff():
                    self.powder_pattern.plot(hkl=True, fig=self.analysis_fig)
            for i in range(len(self.crystal_phases)):
                self.crystal_phases[0]["widget"].update_lattice()  # manual update of the lattice in the widget
        except Exception as ex:
            self.logger.error(traceback.format_exc())
        self.logger.info("Profile fitting... done")

    async def periodic_check(self):
        while self.toggle_live.value:
            v_i = self.scan_data_folder()
            self.logger.debug(f"Checking for new data.... {max(self.v_i)}< {v_i[-1]} ?")
            if max(self.v_i) < v_i[-1]:
                nb = 0
                with NoEventContextManager(self):
                    try:
                        for i in v_i:
                            if i not in self.v_i:
                                self.slider_frame.value = i
                                self.load_i(update_plot=False if i < v_i[-1] else True)
                                nb += 1
                    except Exception:
                        self.slider_frame.max = max(self.v_i)
                        self.logger.info(f'Error loading new frame; will try again')
                if nb > 10:
                    self.logger.info(f"Loaded {nb} new frames ; now at #{self.v_i[-1]}")
            else:
                self.slider_frame.value = self.slider_frame.max
            self.log_handler.crop_logs(nb=100)  # Don't accumulate too much log
            await asyncio.sleep(1)

    def run_live(self):
        self.logger.info("Running live update loop")
        self.loop = asyncio.get_event_loop()
        self.loop_task = self.loop.create_task(self.periodic_check())
        # self.loop.run_until_complete(self.loop_task)
        self.logger.info("Running live update loop-DONE")

    def init_power_pattern(self):
        """Create the powder pattern object, remove all crystal phases"""
        if self.azi is None:
            return
        # Remove existing phases, if any
        for ic in range(len(self.crystal_phases)):
            try:
                # This is only necessary if the 1D graph already exists and was not reset
                self.crystal_phases[ic]['scatter'].remove()
                self.crystal_phases[ic]['text'].remove()
            except Exception:
                pass
        self.crystal_phases = []
        self.crystal_vbox.children = [self.crystal_tuning_toggle]
        self._init_powder_pattern()
        self.logger.info("Initialised powder pattern object")

    def _init_powder_pattern(self):
        """Init the powder pattern"""
        # keep colours in case they were already changed
        if self.powder_pattern is not None:
            vc = self.powder_pattern._colour_phases
        else:
            vc = ['red', 'green', 'blue', 'magenta', 'orange', "olive",
                  "cyan", "purple", "magenta", "salmon"]

        self.powder_pattern = PowderPattern()
        if vc is not None:
            self.powder_pattern._colour_phases = vc

        self.powder_pattern.SetWavelength(self.azi.get_wavelength() * 1e10)  # In Angstroems for pyobjcryst
        tth = np.deg2rad(self.tth)
        self.powder_pattern.SetPowderPatternX(tth)
        self.powder_pattern.SetPowderPatternObs(self.iobs2d[self.slider_frame.value].astype(np.float64))
        # Add powder pattern background with 20 interpolation points
        # It is left at zero, available for manual use
        bx = np.linspace(tth.min(), tth.max(), 20)
        by = np.zeros(bx.shape)
        self.powderpattern_background = self.powder_pattern.AddPowderPatternBackground()
        self.powderpattern_background.SetInterpPoints(bx, by)

    def _update_powder_pattern_phases(self):
        """ Update the list of displayed crystalline phases
        in the powder pattern, based on the toggles"""
        # TODO: it would be easier to have a RemovePowderPatternComponent() function,
        #  and less prone to memory leaks (deletion of powder pattern is probably not clean)
        self.logger.info("Re-init powder pattern to update crystalline phases")
        # First get the XML so we can reload them
        xml_background = self.powderpattern_background.xml()
        xml_dict = {}
        for ic in range(len(self.crystal_phases)):
            wc = self.crystal_phases[ic]
            if wc['diffraction'] is not None:
                xml_dict[wc['crystal'].int_ptr()] = wc['diffraction'].xml()

        # Re-init powder pattern, add crystal phases and reload available phases
        self._init_powder_pattern()
        self.powderpattern_background.XMLInput(xml_background)  # probably not needed
        for ic in range(len(self.crystal_phases)):
            wc = self.crystal_phases[ic]
            if wc["toggle"].value:
                pd = self.powder_pattern.AddPowderPatternDiffraction(wc['crystal'])
                pd.XMLInput(xml_dict[wc['crystal'].int_ptr()])
                wc['diffraction'] = pd
        self.logger.info("Re-init powder pattern to update crystalline phases...done")

    def add_crystal_cif(self, cif):
        if self.powder_pattern is None:
            if cif not in self.config_crystals.value:
                if len(self.config_crystals.value):
                    self.config_crystals.value += "\n"
                self.config_crystals.value += f"{cif}"
        else:
            self.logger.info("Adding crystal phase with CIF: %s" % cif)
            try:
                t0 = socket.getdefaulttimeout()  # should use a context manager
                socket.setdefaulttimeout(5)
                try:
                    c = create_crystal_from_cif(cif)
                    socket.setdefaulttimeout(t0)
                except URLError:
                    self.logger.error(f"Timeout (5s) trying to get CIF from: {cif} - check the URL ? ")
                    socket.setdefaulttimeout(t0)
                    return
            except Exception:
                self.logger.error("Error creating crystal from CIF: %s . Check path/URL ?" % cif)
                return
            self.add_crystal(c)
            self.crystal_phases[-1]["cif"] = cif

    def add_crystal(self, c):
        self.logger.info("Adding crystal phase...  name: %s" % c.GetName())
        self.logger.info(
            "Adding crystal phase->lattice: %s, spacegroup: %s" % (str(c).split('\n')[1], str(c.GetSpaceGroup())))
        pd = self.powder_pattern.AddPowderPatternDiffraction(c)
        # Create widget
        wc = CrystalWidget(c, self)
        toggle = ipywidgets.ToggleButton(value=True, description='', icon='check',
                                         layout=ipywidgets.Layout(width='2em', height='auto'),
                                         description_tooltip="Toggle the phase to add/remove it "
                                                             "completely from the display and analysis")
        toggle.observe(self.on_toggle_crystal)
        accordion = ipywidgets.Accordion(children=[wc], titles=(c.GetName(),))
        box = ipywidgets.HBox()
        box.children = toggle, accordion
        self.crystal_vbox.children = list(self.crystal_vbox.children) + [box]
        self.crystal_phases.append({})
        self.crystal_phases[-1]["crystal"] = c  # the Crystal object
        self.crystal_phases[-1][
            "diffraction"] = pd  # the PowderPatternDiffraction object corresponding to this crystal phase
        self.crystal_phases[-1]["widget"] = wc
        self.crystal_phases[-1]["cif"] = None  # URL or path to the CIF file, if available
        self.crystal_phases[-1]["toggle"] = toggle  # toggle button to hide or display reflections in the widget plot
        self.crystal_phases[-1]["accordion"] = accordion  # show/hide crystal lattice and spacegroup
        self.crystal_phases[-1]["scatter"] = None  # scatterplot used to display reflection positions
        vc = self.powder_pattern._colour_phases
        wc.color_picker.value = vc[(len(self.crystal_phases) - 1) % len(vc)]
        self.update_1d()
        self.apply_crystal_tuning()

    def load_config(self, filename="id06lvp_widget_config.json"):
        """Load config from json file if available"""
        if self.poni_filename is None and self.data_src is None:
            if os.path.exists(filename):
                try:
                    config = json.load(open(filename))
                    if "processed_data_filename" in config:
                        if config["processed_data_filename"] is not None:
                            self.config_processed_data_filename.value = config["processed_data_filename"]
                            self.refresh_processed_available_scans()
                    if "processed_data_scan" in config:
                        if config["processed_data_scan"] is not None:
                            n = config["processed_data_scan"]
                            try:
                                self.config_processed_scan.value = n
                            except Exception as ex:
                                self.logger.error(f"Failed selecting scan #{n} - is that scan available in "
                                                  f"{self.config_processed_data_filename.value} ?")
                    if "poni_filename" in config:
                        if config["poni_filename"] is not None:
                            self.config_poni_file.value = config["poni_filename"]
                    if "detector_geometry_filename" in config:
                        if config["detector_geometry_filename"] is not None:
                            self.config_detector_file.value = config["detector_geometry_filename"]
                    if "data_src" in config:
                        if config["data_src"] is not None:
                            self.config_data_src.value = config["data_src"]
                    if "cifs" in config:
                        if config["cifs"] is not None:
                            self.config_crystals.value = config["cifs"]
                    self.logger.error("Loaded previous config from: %s" % filename)
                except Exception:
                    self.logger.error(traceback.format_exc())
                    self.logger.error("Failed loading previous config from: %s" % filename)

    def save_config(self, filename="id06lvp_widget_config.json"):
        config = {"processed_data_filename": self.processed_data_filename,
                  "processed_data_scan": self.processed_data_scan,
                  "poni_filename": self.poni_filename, "data_src": self.data_src,
                  "cifs": self.config_crystals.value,
                  "detector_geometry_filename": self.detector_geometry_filename}
        json.dump(config, open(filename, "w"))
        self.logger.info("Saved configuration to: %s" % filename)

    def on_analysis_pressure(self, v):
        """Perform the pressure calculation from the selected options and crystal phase"""
        if v is not self.pressure_button:
            if v['name'] == 'value' and (len(self.pressure_phase.options) == 0 or self.pressure_phase.index is None):
                # Method has been selected, or temperature changed, before the crystalline phase, just ignore
                # Only give an error message when a button is clicked
                return
        if len(self.pressure_phase.options) == 0:
            self.logger.error("Cannot compute pressure unless there is a fitted crystalline phase")
            return
        if self.pressure_phase.index is None:
            self.logger.error("You must select a (fitted) crystalline phase to compute the pressure")
            return
        c = self.crystal_phases[self.pressure_phase.index]['crystal']
        v = c.GetVolume()
        tmp = self.pressure_method.value.split('-')
        self.logger.debug(f"Pressure calc: crystal={c.GetName()} v={v:.2f} method={self.pressure_method.value}")
        eos = tmp[0]
        st = tmp[1] if len(tmp) > 1 else None
        temp = self.pressure_temp.value
        p = pressure_calc(eos, st, v, temp)
        i = self.slider_frame.value
        self.logger.info(f"P[{i},{tmp}]: V[{c.GetName()}]={v:.1f}A^3 T={temp:.1f}K => P={p:.3f} GPa")
        self.pressure_out.value = f"{p:.3f}"
        self._pressure_values[i] = p
        # plot
        x = np.fromiter(self._pressure_values.keys(), dtype=float)
        y = np.fromiter(self._pressure_values.values(), dtype=float)
        if True:  # with plt.ioff():
            if self.plot_pressure_obj is None:
                self.plot_pressure_ax = self.plot_pressure_fig.subplots()
                self.plot_pressure_obj = self.plot_pressure_ax.plot(x, y, '.')[0]
                self.plot_pressure_ax.grid(True)
                self.plot_pressure_ax.set_xlabel('frame#', labelpad=-2, loc='right', fontsize=9)
                self.plot_pressure_ax.set_ylabel('P[GPa]', labelpad=0, loc='top', fontsize=9)
            else:
                self.plot_pressure_obj.set_data(x, y)
            self.plot_pressure_ax.set_xlim(0, len(self.iobs2d) * 1.05)
            self.plot_pressure_ax.set_ylim(0, y.max() * 1.05)
