# -*- coding: utf-8 -*-

#   (c) 2022-present : ESRF-European Synchrotron Radiation Facility
#       authors:
#         Vincent Favre-Nicolin, favre@esrf.fr

"""Useful classes for custom widgets"""

__all__ = ['NoEventContextManager', 'OutputWidgetHandler']

import ipywidgets
import logging
from IPython.display import display


class NoEventContextManager:
    """Context manager to disable events in widgets"""

    def __init__(self, obj):
        """

        :param obj: the widget for which the events will be disabled
        """
        self.obj = obj

    def __enter__(self):
        self.obj._enable_events = False

    def __exit__(self, exc_type, exc_value, exc_tb):
        self.obj._enable_events = True


class OutputWidgetHandler(logging.Handler):
    """ Custom logging handler sending logs to an output widget
    Original code: https://ipywidgets.readthedocs.io/en/7.6.5/examples/Output%20Widget.html
    """

    def __init__(self, *args, **kwargs):
        super(OutputWidgetHandler, self).__init__(*args, **kwargs)
        layout = {
            'width': '100%',
            'height': '160px',
            'border': '1px solid black'
        }
        self.out = ipywidgets.Output(layout=layout)

    def emit(self, record):
        """ Overload of logging.Handler method """
        formatted_record = self.format(record)
        new_output = {
            'name': 'stdout',
            'output_type': 'stream',
            'text': formatted_record + '\n'
        }
        self.out.outputs = (new_output,) + self.out.outputs

    def show_logs(self):
        """ Show the logs """
        display(self.out)

    def clear_logs(self):
        """ Clear the current logs """
        self.out.clear_output()

    def crop_logs(self, nb=20):
        if len(self.out.outputs) > nb:
            self.out.outputs = self.out.outputs[:20]
