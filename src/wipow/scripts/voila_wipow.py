import os
import argparse
import shutil


def get_paths_config():
    pth = os.path.dirname(os.path.abspath(__file__))
    cwd = os.getcwd()
    v = f"WIPOW_CWD={cwd}"
    return pth, cwd, v


def make_parser(name):
    """
    Script to launch wipow [https://gitlab.esrf.fr/favre/wipow]
    """
    pth, cwd, v = get_paths_config()
    epilog = """"""

    parser = argparse.ArgumentParser(prog=name,
                                     description=__doc__,
                                     epilog=epilog)
    parser.add_argument("--copy", action='store_true',
                        help="If used, the notebook used to launch wipow will be copied "
                             "in the current directory, so that it can be modified.")
    parser.add_argument("--notebook", type=str,
                        default=os.path.join(pth, 'voila_wipow.ipynb'),
                        help="path to the notebook to use (default: the one in the wipow package)")
    return parser


def parse_args(name):
    params = make_parser(name).parse_args()
    if params.copy:
        shutil.copy(params.notebook, os.getcwd())
        params.notebook = os.path.join(os.getcwd(), os.path.split(params.notebook)[-1])
    return params


def run_voila():
    pth, cwd, v = get_paths_config()
    nbk = parse_args('voila').notebook
    os.system(f"{v} voila {nbk}")


def run_notebook():
    pth, cwd, v = get_paths_config()
    nbk = parse_args('voila-notebook').notebook
    os.system(f"{v} jupyter-notebook {nbk}")


def run_lab():
    pth, cwd, v = get_paths_config()
    nbk = parse_args('voila-lab').notebook
    os.system(f"{v} jupyter-lab {nbk}")


if __name__ == '__main__':
    run_voila()
